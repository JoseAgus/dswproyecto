<?php

require_once "database/IEntity.php";
class ImagenGaleria implements IEntity
{
    private $id;

    private $nombre;

    private $descripcion;


    const RUTA_IMAGENES_PORTFOLIO = "img/";
    public const RUTA_IMAGENES_GALLERY = "img/";




    public function __construct($id = 0, string  $nombre = "",  string $descripcion = "")
    {
        $this->id = $id;

        $this->nombre = $nombre;

        $this->descripcion = $descripcion;
    }

    public function getURLPortfolio(): string
    {

        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();
    }

    public function getURLGallery(): string
    {

        return self::RUTA_IMAGENES_GALLERY . $this->getNombre();
    }


    public function getId()
    {
        return $this->id;
    }
    public function getNombre()
    {
        return $this->nombre;
    }
    public function getDescripcion()
    {
        return $this->descripcion;
    }



    public function toArray(): array

    {

        return [

            "id" => $this->getId(),

            "nombre" => $this->getNombre(),

            "descripcion" => $this->getDescripcion()

        ];
    }
}
