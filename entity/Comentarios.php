<?php

require_once "database/IEntity.php";

class Opiniones implements IEntity
{

    private $id;
    private $nombre;
    private $apellidos;
    private $email;
    private $mensaje;
    private $valo ;


    public function __construct($id = 0, string  $nombre = "",  string $apellidos = "", string $email = "", string $mensaje = "", int $valo = 1)
    {

        $this->id = $id;
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->email = $email;
        $this->mensaje = $mensaje;
        $this->valo = $valo;
    }

    public function getId()
    {
        return $this->id;
    }
    public function getNombre()
    {
        return $this->nombre;
    }
    public function getApellidos()
    {
        return $this->apellidos;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getmensaje()
    {
        return $this->mensaje;
    }
    public function getValoracion()
    {
        return $this->valo;   
    }    



    public function toArray(): array

    {

        return [

            "id" => $this->getId(),

            "nombre" => $this->getNombre(),

            "apellidos" => $this->getApellidos(),

            "email" => $this->getEmail(),

            "mensaje" => $this->getmensaje(),

            "valoracion" => $this->getValoracion()

        ];
    }
}
