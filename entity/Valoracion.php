<?php
require_once "database/IEntity.php";
class Valoracion implements IEntity
{

    private $id;
    private $estrellas;


    public function __construct($id = 0, string  $estrellas = "")
    {

        $this->id = $id;
        $this->nombre = $estrellas;
    }

    public function getIdV()
    {
        return $this->id;
    }
    public function getNombreV()
    {
        return $this->estrellas;
    }




    public function toArray(): array

    {

        return [

            "id" => $this->getIdV(),

            "nombre" => $this->getNombreV()

        ];
    }
}
