<?php

class Dbconnec
{


    public static function make()
    { //Hace la coneccion a la base de datos

        try {

            $config = App::get("config")["database"];

            $connection = new PDO(

                $config["connection"] . ";dbname=" . $config["name"],

                $config["username"],

                $config["password"],

                $config["options"]
            );
        } catch (PDOException $PDOException) {

            throw new AppException("No se ha podido conectar con la BBDD");
        }

        return $connection;
    }
}
