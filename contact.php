

<?php

require_once "utils/File.php";

require_once "exceptions/FileException.php";

require_once "exceptions/QueryException.php";

require_once "entity/ImagenGaleria.php";

require_once "entity/Comentarios.php";

require_once "entity/Valoracion.php";

require_once "database/Dbconnec.php";

require_once "database/QueryBuilder.php";

require_once "core/App.php";

require_once "repository/ComentariosRepository.php";

require_once "repository/ValoracionRepository.php";


//Filtro del email para que no entre en la base de datos si no es un email
$emailx = isset($_POST['email']) ? $_POST['email'] : null;
function validaEmailX($valor){

    if(filter_var($valor, FILTER_VALIDATE_EMAIL) === FALSE){
       return false;
    }else{
       return true;
    }

}

try {


    $config = require_once("app/config.php");

    App::bind("config", $config);

    // $connection = App::getConnection();

    $ValoracionRepository = new ValoracionRepository();
    $ComentariosRepository = new ComentariosRepository();

    if ($_SERVER["REQUEST_METHOD"] === "POST") {


        $nombre = trim(htmlspecialchars($_POST["name"]));
        $apellidos = trim(htmlspecialchars($_POST["lastname"]));
        $email = trim(htmlspecialchars($_POST["email"]));
        $texto = trim(htmlspecialchars($_POST["message"]));
        $valoracion = trim(htmlspecialchars($_POST["valoracion"]));

        

        $mensaje = "Datos enviados";

        $Opiniones = new Opiniones(0, $nombre, $apellidos, $email, $texto, $valoracion);

        if (!$_POST["name"] == "" && !$_POST["email"] == "" && !$_POST["message"] == "") {
            if (validaEmailX($emailx)) {//Solo se introduce en la base de datos si comple lo del formulario

                $ComentariosRepository->save($Opiniones);

                $mensaje = "Se a guardado en la db";
                

            }
        }
    }
    $valoraciones = $ValoracionRepository->findAll();
    $Comentario = $ComentariosRepository->findAll();
} catch (FileException $fileException) {

    $errores[] = $fileException->getMessage();
} catch (QueryException $queryException) {

    $errores[] = $queryException->getMessage();
} catch (AppException $AppException) {

    $errores[] = $AppException->getMessage();
}


require_once "views/contact.view.php";
