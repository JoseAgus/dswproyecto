<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
// Create the logger
$log = new Logger('galeria');
// Now add some handlers
$log->pushHandler(new StreamHandler('logs/info.log', Logger::INFO));

//Filtro del email para que no entre en la base de datos si no es un email
$emailx = isset($_POST['email']) ? $_POST['email'] : null;
function validaEmailX($valor){

    if(filter_var($valor, FILTER_VALIDATE_EMAIL) === FALSE){
       return false;
    }else{
       return true;
    }

}

try {


    // $config = require_once("app/config.php");

    // App::bind("config", $config);

    $ValoracionRepository = new ValoracionRepository();
    $ComentariosRepository = new ComentariosRepository();

    if ($_SERVER["REQUEST_METHOD"] === "POST") {


        $nombre = trim(htmlspecialchars($_POST["name"]));
        $apellidos = trim(htmlspecialchars($_POST["lastname"]));
        $email = trim(htmlspecialchars($_POST["email"]));
        $texto = trim(htmlspecialchars($_POST["message"]));
        $valoracion = intval(trim(htmlspecialchars($_POST["valoracion"])));

    

        $mensaje = "Datos enviados";
        $Opiniones = new Opiniones(0, $nombre, $apellidos, $email, $texto,   $valoracion);

        if (!$_POST["name"] == "" && !$_POST["email"] == "" && !$_POST["message"] == "") {
            if (validaEmailX($emailx)) {//Solo se introduce en la base de datos si comple lo del formulario

                $ComentariosRepository->save($Opiniones);

                $mensaje = "La opinion se a guardado en la BBDD";

                $log->info($mensaje);
                // App::get("logger")->add($mensaje);

            }
        }
    }
    $valoraciones = $ValoracionRepository->findAll();
    $Comentario = $ComentariosRepository->findAll();
}catch (FileException $fileException) {

    $errores[] = $fileException->getMessage();

}catch (QueryException $queryException) {

    $errores[] = $queryException->getMessage();

}catch (AppException $AppException) {

    $errores[] = $AppException->getMessage();

}catch (NotFoundException $notException) {

    $errores[] = $queryException->getMessage();
    
}

require __DIR__ . "/../views/contact.view.php";