<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
// Create the logger
$log = new Logger('galeria');
// Now add some handlers
$log->pushHandler(new StreamHandler('logs/info.log', Logger::INFO));

try {

    $imagenGaleriaRepository = new ImagenGaleriaRepository();

    if ($_SERVER["REQUEST_METHOD"] === "POST") {

        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));
        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $imagen = new File("imagen", $tiposAceptados);

        $imagen->saveUploadFile("/var/www/html/Proyecto/dswproyecto/" . ImagenGaleria::RUTA_IMAGENES_GALLERY);

        $mensaje = "Datos enviados";

        $imagenGaleria = new ImagenGaleria(0, $imagen->getFileName(), $descripcion);

        $imagenGaleriaRepository->save($imagenGaleria);

        $mensaje = "La imagen se a guardado en la BBDD";

        $log->info($mensaje);
    }

    $imagenes = $imagenGaleriaRepository->findAll();
} catch (FileException $fileException) {

    $errores[] = $fileException->getMessage();

} catch (QueryException $queryException) {

    $errores[] = $queryException->getMessage();

} catch (PDOException $PDOException) {

    $errores[] = $queryException->getMessage();

} catch (NotFoundException $notException) {

    $errores[] = $queryException->getMessage();
    
}

require __DIR__ . "/../views/galeria.view.php";