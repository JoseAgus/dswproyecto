<?php
include __DIR__ . "/partials/inicio-doc.part.php";

include __DIR__ . "/partials/nav.part.php";
?>
<!-- Principal Content Start -->

<header class="masthead" style="background-image: url('img/relax.jpg')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                    <h1>Gallery</h1>
                    <span class="subheading">A Blog Theme by Start Bootstrap</span>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Subida de archivos -->
<div class="container">
    <div class="row" id="galeria">
        <div class="col-lg-8 col-md-10 mx-auto ">
            <h1>Gallery</h1>
            <hr>
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
                <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                    <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">Close</span>
                    </button>

                    <?php if (empty($errores)) : ?>
                        <p><?= $mensaje ?></p>
                    <?php else : ?>
                        <ul>
                            <?php foreach ($errores as $error) : ?>
                                <li><?= $error ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion"></textarea>
                        <button class="btn btn-lg sr-button btn btn-primary" style="float: right;">ENVIAR</button>
                    </div>
                </div>
            </form>
        </div>
        <br>
        <!-- imagen subida -->
        <div class="text-center col-lg-10 col-md-10 mx-auto">
            <?php if (isset($imagenes)) { ?>
                <?php foreach ($imagenes as $imagen) : ?>

                    <img src="<?= $imagen->getURLGallery() ?>" alt="<?= $imagen->getNombre() ?>" title="<?= $imagen->getDescripcion() ?>" width="300px" style="padding: 10px;">

                <?php endforeach; ?>
            <?php } ?>
        </div>

    </div>
</div>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <ul class="list-inline text-center">
                    <li class="list-inline-item">
                        <a href="#">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://www.youtube.com/watch?v=brNGDA5R048&ab_channel=LeagueofLegends">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://bitbucket.org/JoseAgus/dswproyecto/src/master/">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </li>
                </ul>
                <p class="copyright text-muted">Copyright &copy; Your Website 2020</p>
            </div>
        </div>
    </div>
</footer>
<!-- Principal Content End -->
<?php include __DIR__ . "/partials/fin-doc.part.php"; ?>