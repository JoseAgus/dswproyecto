<?php
include("functions.php");

$nombre = isset($_POST['name']) ? $_POST['name'] : null;
$email = isset($_POST['email']) ? $_POST['email'] : null;
$mensaje = isset($_POST['message']) ? $_POST['message'] : null;

$errores = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {


  if (!validaNombre($nombre)) {
    array_push($errores, 'El campo nombre es incorrecto.');
  }

  if (!validaEmail($email)) {
    array_push($errores, 'El campo email es incorrecto.');
  }

  if (!validaMensaje($mensaje)) {
    array_push($errores, 'El campo mensaje es incorrecto.');
  }
}
?>



<?php include __DIR__ . "/partials/inicio-doc.part.php"; ?>

<?php include __DIR__ . "/partials/nav.part.php"; ?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('img/contact-bg.jpg')">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="page-heading">
          <h1>Opina</h1>
          <span class="subheading">Danos tu punto de vista.</span>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- Main Content -->
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">
      <p>¿Quieres Opinar? Complete el siguiente formulario para comentar sobre la pagina.</p>

      <form method="post">
        <div class="control-group">
          <div class="form-group floating-label-form-group controls">
            <h5>Nombre</h5>
            <input type="text" class="form-control" placeholder="nombre" name="name">

            <?php if ($errores) : ?>
              <ul style="color: red;">
                <?php foreach ($errores as $error) :
                  if ($error != 'El campo nombre es incorrecto.')
                    continue;
                ?>
                  <li>
                    <?php echo $error; ?>
                  </li>
                <?php endforeach; ?>
              </ul>
            <?php endif; ?>

            <p class="help-block text-danger"></p>
          </div>
        </div>
        <div class="control-group">
          <div class="form-group floating-label-form-group controls">
            <h5>Apellidos</h5>
            <input type="text" class="form-control" placeholder="apellidos" name="lastname">
            <p class="help-block text-danger"></p>
          </div>
        </div>
        <div class="control-group">
          <div class="form-group floating-label-form-group controls">
            <h5>Direccion Email</h5>
            <input type="text" class="form-control" placeholder="email" name="email">

            <?php if ($errores) : ?>
              <ul style="color: red;">
                <?php foreach ($errores as $error) :
                  if ($error != 'El campo email es incorrecto.')
                    continue;
                ?>
                  <li>
                    <?php echo $error; ?>
                  </li>
                <?php endforeach; ?>
              </ul>
            <?php endif; ?>

            <p class="help-block text-danger"></p>
          </div>
        </div>
        <div class="control-group">
          <div class="form-group floating-label-form-group controls">
            <h5>Mensaje</h5>
            <textarea rows="5" class="form-control" placeholder="mensaje" name="message"></textarea>

            <?php if ($errores) : ?>
              <ul style="color: red;">
                <?php foreach ($errores as $error) :
                  if ($error != 'El campo mensaje es incorrecto.')
                    continue;
                ?>
                  <li>
                    <?php echo $error; ?>
                  </li>
                <?php endforeach; ?>
              </ul>
            <?php endif; ?>

            <p class="help-block text-danger"></p>
          </div>
        </div>

        <div class="control-group">
          <div class="form-group floating-label-form-group controls">
            <h5>Valoración</h5>
            <select class="form-control" name="valoracion">
              <?php foreach ($valoraciones as $valoracion) : ?>

                <option value="<?= $valoracion->getIdV() ?>"> <?= $valoracion->getNombreV(); ?></option>

              <?php endforeach; ?>
            </select>
            <p>(Pss, valora alto eh)</p>
          </div>
        </div>

        <br>
        <div id="success"></div>
        <button name="Submit" class="btn btn-primary">Send</button>
      </form>
      <br>
      <br>


      <!-- <table class="table"> -->
        <?php foreach ($Comentario as $Comen) : ?>

          <!-- <tr>

            <th scope="row"><?= $Comen->getId() ?></th>

            <th scope="col">Valoracion</th>

            <td class="text-center"><?= $Comen->Valoracion ?> Estrella</td>

            <td><?= $Comen->getNombre() ?></td>

            <td><?= $Comen->getApellidos() ?></td>

            <td><?= $Comen->getEmail() ?></td>

            <td>Mensaje: <?= $Comen->getmensaje() ?></td>


          </tr> -->
          <p class="text-center" style="font-size: 12px;">Comentario: <?= $Comen->getId() ?></p>
          <hr>
          <div class="border p-4">
            <h5><?= $Comen->getNombre() ?> <?= $Comen->getApellidos() ?> </h5>
            <h6><?= $Comen->Valoracion ?> Estrella</h6>
            <i><?= $Comen->getEmail() ?></i>
            <p><?= $Comen->getmensaje() ?></p>
          </div>
          <hr>
        <?php endforeach; ?>
      <!-- </table> -->

    </div>
  </div>
</div>

<hr>

<!-- Footer -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <ul class="list-inline text-center">
          <li class="list-inline-item">
            <a href="#">
              <span class="fa-stack fa-lg">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </li>
          <li class="list-inline-item">
            <a href="https://www.youtube.com/watch?v=brNGDA5R048&ab_channel=LeagueofLegends">
              <span class="fa-stack fa-lg">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </li>
          <li class="list-inline-item">
            <a href="https://bitbucket.org/JoseAgus/dswproyecto/src/master/">
              <span class="fa-stack fa-lg">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fab fa-github fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </li>
        </ul>
        <p class="copyright text-muted">Copyright &copy; Your Website 2020</p>
      </div>
    </div>
  </div>
</footer>

<?php include __DIR__ . "/partials/fin-doc.part.php"; ?>