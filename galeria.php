<?php

require_once "utils/File.php";

require_once "exceptions/FileException.php";

require_once "exceptions/QueryException.php";

require_once "entity/ImagenGaleria.php";

require_once "database/Dbconnec.php";

require_once "database/QueryBuilder.php";

require_once "core/App.php";

require_once "repository/ImagenGaleriaRepository.php";

try {


    $config = require_once("app/config.php");

    App::bind("config", $config);

    $imagenGaleriaRepository = new ImagenGaleriaRepository();

    if ($_SERVER["REQUEST_METHOD"] === "POST") {

        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));
        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $imagen = new File("imagen", $tiposAceptados);

        $imagen->saveUploadFile("/var/www/html/Proyecto/dswproyecto/" . ImagenGaleria::RUTA_IMAGENES_GALLERY);

        $mensaje = "Datos enviados";

        $imagenGaleria = new ImagenGaleria(0, $imagen->getFileName(), $descripcion);

        $imagenGaleriaRepository->save($imagenGaleria);

        $mensaje = "Se a guardado en la db";
    }

    $imagenes = $imagenGaleriaRepository->findAll();
} catch (FileException $fileException) {

    $errores[] = $fileException->getMessage();
} catch (QueryException $queryException) {

    $errores[] = $queryException->getMessage();
} catch (PDOException $PDOException) {

    throw new AppException("No se ha podido conectar con la BBDD");
}

require_once "views/galeria.view.php";
