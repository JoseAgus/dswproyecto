

<?php 

require_once "App.php";

require_once "Request.php";

require_once "utils/File.php";

require_once "exceptions/FileException.php";

require_once "exceptions/QueryException.php";

require_once "entity/ImagenGaleria.php";

require_once "entity/Comentarios.php";

require_once "entity/Valoracion.php";

require_once "database/Dbconnec.php";

require_once "database/QueryBuilder.php";

require_once "core/App.php";

require_once "repository/ComentariosRepository.php";

require_once "repository/ValoracionRepository.php";

require_once "repository/ImagenGaleriaRepository.php";

require_once "vendor/autoload.php";

require_once "utils/MyLog.php";


$config = require_once __DIR__ . "/../app/config.php";

App::bind("config", $config);

?>


